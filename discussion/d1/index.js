console.log("Hello World");

/* OBJECTS */
/*
	- data types used to represent real world objects
	- collection of related data
	- JS object has "key: value" pair

	SYNTAX:
		let/const objectName = {
		keyA: ValueA,
		KeyB: ValueB
		}

	* OBJECT LITERALS = {}
	** ARRAY LITERALS = []
*/
let cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}

console.log("Result from creating OBJECT using initializers");
console.log(cellphone); // print object
console.log(typeof cellphone);// determines data type

/*MINIACTIVTY*/
/*
	create object called car
		name/model
		release date/year released
*/

let car = {
	model: "Public Utility Jeepney",
	releaseDate: "18XX"
}

console.log("Result from creating OBJECT 'car' using initializers");
console.log(car); // print object
console.log(typeof car);// determines data type

// CONSTRUCTOR FUNCTION
// useful for creating multiple copies/ similar objects

function Laptop(name, manufacturedDate) {
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}

let laptop = new Laptop("Dell",2012);
console.log("Result from creating OBJECT 'laptop' using constructor");
console.log(laptop);
console.log(typeof laptop);

let laptop2 = new Laptop("Lenovo",2008);
console.log("Result from creating OBJECT 'laptop2' using constructor");
console.log(laptop2);
console.log(typeof laptop2);

let computer = {};
let myComputer = new Object();
console.log("Result from creating OBJECT 'myComputer' using constructor");
console.log(myComputer);
console.log(typeof myComputer);

// ACCESSING OBJECTS

console.log("Result from dot notation: "+laptop.name);
console.log("Result from dot notation: "+laptop2.manufacturedDate);

let laptops = [laptop, laptop2];
console.log(laptops[1].name);
console.log(laptops[0].manufacturedDate);
console.log(laptops[0]["manufacturedDate"]); //access property , prefer to use dot notation

car = {};
console.log(car);

// SECTION - init, add, del, reass obj properties
// basically acts like variables 

car.name = "Honda Vios";
car.manufacturedDate = 2022;
console.log(car);

car["manufactured date"] = 2019;
console.log(car);

// deleting of properties
delete car["manufactured date"];
// delete car.manufacturedDate; - can also be used with dot notation
console.log(car);

// reassigning of properties
/*
	change the name of the car into "Dodge Charger R/T"
*/
car.name = "Dodge Charger R/T";
console.log(car);

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is "+this.name)
	}
}

console.log(person);
console.log("Result from object methods:");
person.talk();
person.walk = function() {
	console.log(this.name+" walked 25 steps.")
	
}
person.walk();

// TEMPLATE LITERALS `$ + stringData
// (`${this.name} walked 25 steps`);
// basically replaces the ADDITION

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		state: "Texas"
	},
	emails: ["joe@mail.com","johnHandsome@mail.com"],
	introduce: function(){
        console.log(`Hello! My name is ${this.firstName} ${this.lastName}`);
    }
}
friend.introduce();
console.log(friend);

// Hello! My name is <fn> + " " + <last> + "."

/*
	Scenario
		- we would like to create a game that would have several pokemon interact with each other
		- every pokemon would have the same set of stats, properties and functions
*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log( "This Pokemon tackled targetPokemon" );
		console.log( "targetPokemon's health is now reduced to _targetPokemonHealth_" );
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon);

// using constructor function
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.atttack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	};
	this.faint = function() {
		console.log(this.name + " fainted.");
	}
}
// creating new pokemon
let pikachu = new Pokemon ("Pikachu", 16);
let rattata = new Pokemon ("Rattata", 8);

// using the tackle method from pikachu with rattata as its target
pikachu.tackle(rattata);