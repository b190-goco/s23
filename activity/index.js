
/*Activity:
1. In the S23 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)*/
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	talk: function() {
	console.log("Pikachu! I choose you!");
	}
}
/*
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
*/
console.log(trainer);
console.log("Result of DOT Notation:");
console.log(trainer.name);
console.log("Result of SQUARE BRACKET Notation:");
console.log(trainer["pokemon"]);

trainer.talk();


/*
8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
*/
function Pokemon(name, level){
	// POKEMON ATTRIBUTES
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	/*// POKEMON MOVES
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to "+(target.health-this.);
	};
	this.faint = function() {
		console.log(this.name + " fainted.");
	}*/
	this.tackle = function(target){
	console.log(this.name + " tackled " + target.name);
	
	// console.log(target.health);
	// console.log(this.attack);

	target.health = target.health - this.attack;
	console.log(target.name + "'s' health is now reduced to "+ target.health);
	target.faint();
	console.log(target);
	};

	this.faint = function() {
		if (this.health < 0) {
			console.log(this.name + " fainted");
		}
	}
}

/*
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
*/
// POKEMON MOVES
/*
tackle = function(target){
	console.log(this.name + " tackled " + target.name);
	console.log(target.name + " health is now reduced to "+(target.health-this.attack));
	console.log(target);
	faint();
};

faint = function() {
	if (this.health < 0) {
		console.log(this.name + " fainted");
	}
}*/
// creating new pokemon
let pikachu = new Pokemon ("Pikachu", 12);
let geodude = new Pokemon ("Geodude", 8);
let mewtwo = new Pokemon ("Mewtwo", 100);

/*let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log( "This Pokemon tackled targetPokemon" );
		console.log( "targetPokemon's health is now reduced to _targetPokemonHealth_" );
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};*/


/*
13. Invoke the tackle method of one pokemon object to see if it works as intended.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
*/
/*14. Create a git repository named S23.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.*/

// using constructor function
